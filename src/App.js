import React from 'react';
import './App.scss';
import ProductList from "./components/ProductList/ProductList";

function App() {
    return (
        <div className="App">
            <header className="menu">
                <a href="https://www.fragrantica.com" className="site-logo">
                    <h1><span className="logoName">N-Parfumery</span></h1>
                </a>
                <div className="menu-links">
                    <a href="https://www.fragrantica.com" className="menu-link">Home</a>
                    <a href="https://www.fragrantica.com" className="menu-link">Products</a>
                    <a href="https://www.fragrantica.com" className="menu-link">About Us</a>
                    <a href="https://www.fragrantica.com" className="menu-link">Contact</a>
                </div>
            </header>
            <ProductList/>
        </div>
    );
}

export default App;
