import React, {Component} from 'react';
import './ProductCard.scss';
import StarRatingComponent from 'react-star-rating-component';

class ProductCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rating: 1
        }
    }

    onStarClick(nextValue, prevValue, name,event) {
        this.setState({rating: nextValue});
        localStorage.setItem('favorites',this.props.userInfo.productNumber);
    }

    render() {
        const { rating } = this.state;
        return (
                <div id={this.props.userInfo.productNumber} key={this.props.userInfo.productNumber} className="card">
                    <img className={"Product-Image"} src={this.props.userInfo.imgSrc} alt="Denim Jeans"/>
                    <p className="Product-Name">{this.props.userInfo.name}</p>
                        <p className="price">${this.props.userInfo.price}</p>
                    <h4>Rating: {rating}</h4>
                    <StarRatingComponent
                        name="Rate"
                        starCount={5}
                        value={rating}
                        onStarClick={this.onStarClick.bind(this)}
                    />
                        <p><button onClick={this.props.onClickAlert}>Add to Cart</button></p>
                </div>
              )
    }
}
export default ProductCard;